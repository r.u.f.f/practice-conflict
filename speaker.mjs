import Person from "./person.mjs";

class Speaker extends Person {
    say(phrase) {
        console.log(`"${super.say(phrase)}" very confidently`)
    }
}

class MeetUpManager extends Person {
    say(phrase) {
       return `${super.say(phrase)}`
    }
    announceResults(arr) {
       return `${this.name} as [${this.position}]: Students who successfully passed the test: ${arr}`
    }
}

let studentsArr = ['John', 'Bob', 'Vasya Pupkin'];

let timofey = new MeetUpManager('Timofey', 'teacher');
let john = new Person('John');
let bob = new Person('Bob', 'student');

console.log(timofey.say('Hi!'));
console.log(john.say('Hello!'));
console.log(bob.say('Hello!'));

console.log(timofey.announceResults(studentsArr));
