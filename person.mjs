export default class Person {
    constructor(name = 'human', position = 'guest') {
        this.name = name;
        this.position = position;
    }

    say(phrase) {
        return `${this.name} as [${this.position}]: ${phrase}`;
    }

}

